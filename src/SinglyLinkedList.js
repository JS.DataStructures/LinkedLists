/**
 * Singly Linked List implementation.
 *
 * @author Gautam Bhutani (buildthedesign@gmail.com)
 */

/**
 * An internal class to store the linked list node
 * It stores the value and the next node in the list
 *
 * @class ListNode linked list node
 */
class ListNode {
    constructor(nodeValue = null) {
        this.nodeValue = nodeValue;
        this.nextNode = null;
    }
    /**
     * Get the node value
     *
     * @returns value {Object} node value
     */
    get value() {
        return this.nodeValue;
    }
    /**
     * Set the node value
     *
     * @param nodeValue {Object} node value
     */
    set value(nodeValue) {
        this.nodeValue = nodeValue;
    }
    /**
     * Get the next node
     *
     * @returns node {ListNode} next node
     */
    get next() {
        return this.nextNode;
    }
    /**
     * Set the next node
     *
     * @param nextNode {ListNode} instance of ListNode
     * @throws Error {Error} An error if next node is not an instance of ListNode or is not null
     */
    set next(nextNode) {
        if (nextNode === null || nextNode instanceof ListNode) {
            this.nextNode = nextNode;
        } else {
            throw new Error('Invalid next node instance');
        }
    }
}

/**
 * Singly Linked List implementation
 *
 * @export SinglyLinkedList Singly Linked List
 * @class SinglyLinkedList
 */
export default class SinglyLinkedList {
    constructor() {
        this._head = this._tail = null;
        this._size = 0;
    }
    getListNode = nodeValue => {
        const listNode = new ListNode(nodeValue);
        listNode.delete = () => this.delete(listNode);
        listNode.insertAfter = value => {
            const nextNode = this.getListNode(value);
            nextNode.next = listNode.next;
            listNode.next = nextNode;
            if (listNode === this._tail) {
                this._tail = nextNode;
            }
            this._size++;
        };
        return listNode;
    }
    /**
     * Insert a node as the head
     *
     * @param nodeValue {Object} head node value
     */
    insertHead = (nodeValue = null) => {
        const dataNode = this.getListNode(nodeValue);
        dataNode.next = this._head;
        this._head = dataNode;
        if (this._tail === null) {
            this._tail = this._head;
        }
        this._size++;
    }
    /**
     * Insert a node to the end of the list
     *
     * @param nodeValue {Object} new node value
     */
    insert = (nodeValue = null) => {
        const dataNode = this.getListNode(nodeValue);
        if (this._head === null || this._tail === null) {
            this._head = this._tail = dataNode;
        } else {
            dataNode.next = this._tail.next;
            this._tail.next = dataNode;
            this._tail = this._tail.next;
        }
        this._size++;
    }
    /**
     * Find a value in the list
     *
     * @param nodeValue {Object} value to be searched
     * @returns ListNode {ListNode} node if value is found, null if value is not found
     */
    find = nodeValue => {
        let currentNode = this._head;
        while (currentNode !== null) {
            if (currentNode.value === nodeValue) {
                return currentNode;
            }
            currentNode = currentNode.next;
        }
        return null;
    }
    /**
     * Delete a node
     *
     * @param dataNode {ListNode} node to be deleted
     */
    delete = dataNode => {
        if (dataNode === null || typeof dataNode === 'undefined' || !(dataNode instanceof ListNode)) {
            // No appropriate node passed in.
            return;
        }
        if (dataNode === this._head) {
            // Deletion of first node.
            this._head = this._head.next;
            this._size--;
            return;
        }
        // Iterate through the list until the parent node is found.
        let parentNode = this._head;
        while (parentNode !== null && parentNode.next !== dataNode) {
            parentNode = parentNode.next;
        }
        if (parentNode !== null) {
            // Parent node found.
            parentNode.next = dataNode.next;
            this._size--;
        }
        return;
    }
    /**
     * Get the head node
     *
     * @returns head {ListNode} the head node
     */
    get head() {
        return this._head;
    }
    /**
     * Get the tail node
     *
     * @returns tail {ListNode} the tail node
     */
    get tail() {
        return this._tail;
    }
    /**
     * Get the size of the list
     *
     * @returns size {Int} the size of the list
     */
    get size() {
        return this._size;
    }
}
