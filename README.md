# Linked Lists

## Installation

```
npm install linkedlists
```

## Usage

```
import {SinglyLinkedList, DoublyLinkedList} from 'linkedlists';
```

## API

Node Element (Singly + Doubly)

```value: Get the data value```

```value (value): Set the data value```

```next: Get the next node element```

```next (Node): Set the next node element```

```insertAfter (value): Insert a node after the current node```

```delete: Delete the node element```

Node Element (Doubly)

```previous: Get the previous node element```

```previous (Node): Set the previous node element```

```insertBefore (value): Insert a node before the current node```

Linked List (Singly + Doubly)

```insert (value): Insert a value into the linked list```

```insertHead (value): Insert a value as the first element/node of the linked list```

```delete (node): Delete the particular node element```

```find (value): Find a value in the linked list. Returns a node element```

```head: Get the head node element```

```tail: Get the tail node element```

```size: Get the size of the linked list```