'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Singly Linked List implementation.
 *
 * @author Gautam Bhutani (buildthedesign@gmail.com)
 */

/**
 * An internal class to store the linked list node
 * It stores the value and the next node in the list
 *
 * @class ListNode linked list node
 */
var ListNode = function () {
    function ListNode() {
        var nodeValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

        _classCallCheck(this, ListNode);

        this.nodeValue = nodeValue;
        this.nextNode = null;
    }
    /**
     * Get the node value
     *
     * @returns value {Object} node value
     */


    _createClass(ListNode, [{
        key: 'value',
        get: function get() {
            return this.nodeValue;
        }
        /**
         * Set the node value
         *
         * @param nodeValue {Object} node value
         */
        ,
        set: function set(nodeValue) {
            this.nodeValue = nodeValue;
        }
        /**
         * Get the next node
         *
         * @returns node {ListNode} next node
         */

    }, {
        key: 'next',
        get: function get() {
            return this.nextNode;
        }
        /**
         * Set the next node
         *
         * @param nextNode {ListNode} instance of ListNode
         * @throws Error {Error} An error if next node is not an instance of ListNode or is not null
         */
        ,
        set: function set(nextNode) {
            if (nextNode === null || nextNode instanceof ListNode) {
                this.nextNode = nextNode;
            } else {
                throw new Error('Invalid next node instance');
            }
        }
    }]);

    return ListNode;
}();

/**
 * Singly Linked List implementation
 *
 * @export SinglyLinkedList Singly Linked List
 * @class SinglyLinkedList
 */


var SinglyLinkedList = function () {
    function SinglyLinkedList() {
        var _this = this;

        _classCallCheck(this, SinglyLinkedList);

        this.getListNode = function (nodeValue) {
            var listNode = new ListNode(nodeValue);
            listNode.delete = function () {
                return _this.delete(listNode);
            };
            listNode.insertAfter = function (value) {
                var nextNode = _this.getListNode(value);
                nextNode.next = listNode.next;
                listNode.next = nextNode;
                if (listNode === _this._tail) {
                    _this._tail = nextNode;
                }
                _this._size++;
            };
            return listNode;
        };

        this.insertHead = function () {
            var nodeValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            var dataNode = _this.getListNode(nodeValue);
            dataNode.next = _this._head;
            _this._head = dataNode;
            if (_this._tail === null) {
                _this._tail = _this._head;
            }
            _this._size++;
        };

        this.insert = function () {
            var nodeValue = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            var dataNode = _this.getListNode(nodeValue);
            if (_this._head === null || _this._tail === null) {
                _this._head = _this._tail = dataNode;
            } else {
                dataNode.next = _this._tail.next;
                _this._tail.next = dataNode;
                _this._tail = _this._tail.next;
            }
            _this._size++;
        };

        this.find = function (nodeValue) {
            var currentNode = _this._head;
            while (currentNode !== null) {
                if (currentNode.value === nodeValue) {
                    return currentNode;
                }
                currentNode = currentNode.next;
            }
            return null;
        };

        this.delete = function (dataNode) {
            if (dataNode === null || typeof dataNode === 'undefined' || !(dataNode instanceof ListNode)) {
                // No appropriate node passed in.
                return;
            }
            if (dataNode === _this._head) {
                // Deletion of first node.
                _this._head = _this._head.next;
                _this._size--;
                return;
            }
            // Iterate through the list until the parent node is found.
            var parentNode = _this._head;
            while (parentNode !== null && parentNode.next !== dataNode) {
                parentNode = parentNode.next;
            }
            if (parentNode !== null) {
                // Parent node found.
                parentNode.next = dataNode.next;
                _this._size--;
            }
            return;
        };

        this._head = this._tail = null;
        this._size = 0;
    }
    /**
     * Insert a node as the head
     *
     * @param nodeValue {Object} head node value
     */

    /**
     * Insert a node to the end of the list
     *
     * @param nodeValue {Object} new node value
     */

    /**
     * Find a value in the list
     *
     * @param nodeValue {Object} value to be searched
     * @returns ListNode {ListNode} node if value is found, null if value is not found
     */

    /**
     * Delete a node
     *
     * @param dataNode {ListNode} node to be deleted
     */


    _createClass(SinglyLinkedList, [{
        key: 'head',

        /**
         * Get the head node
         *
         * @returns head {ListNode} the head node
         */
        get: function get() {
            return this._head;
        }
        /**
         * Get the tail node
         *
         * @returns tail {ListNode} the tail node
         */

    }, {
        key: 'tail',
        get: function get() {
            return this._tail;
        }
        /**
         * Get the size of the list
         *
         * @returns size {Int} the size of the list
         */

    }, {
        key: 'size',
        get: function get() {
            return this._size;
        }
    }]);

    return SinglyLinkedList;
}();

exports.default = SinglyLinkedList;