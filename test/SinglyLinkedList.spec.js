import chai from 'chai';
import SinglyLinkedList from '../src/SinglyLinkedList';

const expect = chai.expect;

describe('LinkedLists', () => {
    describe('SinglyLinkedList', () => {
        let singlyLinkedList;
        beforeEach(() => {
            singlyLinkedList = new SinglyLinkedList();
        });
        describe('Insertion Tests', () => {
            it('should be able to construct', () => {
                expect(singlyLinkedList).be.not.null && expect(singlyLinkedList).be.not.undefined;
            });
            it('should construct with null first node', () => {
                expect(singlyLinkedList.head).be.null;
            });
            it('should construct with null last node', () => {
                expect(singlyLinkedList.tail).be.null;
            });
            it('should be able to insert an element as first node', () => {
                singlyLinkedList.insert(1);
                expect(singlyLinkedList.head.value).be.equal(1);
            });
            it('first node post constructor should be the last node', () => {
                singlyLinkedList.insert(1);
                expect(singlyLinkedList.head).be.equal(singlyLinkedList.tail);
            });
            it('first node post constructor should not have a next node', () => {
                singlyLinkedList.insert(1);
                expect(singlyLinkedList.tail.next).be.null;
            });
            it('first node post constructor should result in size 1', () => {
                singlyLinkedList.insert(1);
                expect(singlyLinkedList.size).be.equal(1);
            });
            it('second node should be sibling of first with second value', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                expect(singlyLinkedList.head.next.value).be.equal(2);
            });
            it('second node should be last node with second value', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                expect(singlyLinkedList.tail.value).be.equal(2);
            });
            it('second node should result in size 2', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                expect(singlyLinkedList.size).be.equal(2);
            });
            it('second node inserted after first node should be sibling of first with second value', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                expect(singlyLinkedList.head.next.value).be.equal(2);
            });
            it('second node inserted after first node should be last node with second value', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                expect(singlyLinkedList.tail.value).be.equal(2);
            });
            it('second node inserted after first node should result in size 2', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.insertAfter(2);
                expect(singlyLinkedList.size).be.equal(2);
            });
            it('second node inserted after first node should result in the tail node value to be 2', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.insertAfter(2);
                expect(singlyLinkedList.tail.value).be.equal(2);
            });
            it('second node inserted as head should result in head value being 2', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insertHead(2);
                expect(singlyLinkedList.head.value).be.equal(2);
            });
            it('second node inserted as head should result in head sibling value being 1', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insertHead(2);
                expect(singlyLinkedList.head.next.value).be.equal(1);
            });
            it('second node inserted as head should result in tail value being 1', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insertHead(2);
                expect(singlyLinkedList.tail.value).be.equal(1);
            });
        });

        describe('Find Tests', () => {
            it('Find an existing node', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.insert(3);
                singlyLinkedList.insert(4);
                expect(singlyLinkedList.find(3)).be.not.null;
            });
            it('Find a non-existent node', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.insert(3);
                singlyLinkedList.insert(4);
                expect(singlyLinkedList.find(5)).be.null;
            });
        });

        describe('Update Tests', () => {
            it('Update an existing node', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.insert(3);
                singlyLinkedList.insert(4);
                const dataNode = singlyLinkedList.find(3);
                dataNode.value = 5;
                expect(singlyLinkedList.find(5)).be.not.null;
            });
        });

        describe('Delete Tests', () => {
            it('Delete first and only node - first node should be null', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.delete(singlyLinkedList.head);
                expect(singlyLinkedList.head).be.null;
            });
            it('Delete first and only node - size should be 0', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.delete(singlyLinkedList.head);
                expect(singlyLinkedList.size).be.equal(0);
            });
            it('Delete first node out of two - second node should become first node', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.delete(singlyLinkedList.head);
                expect(singlyLinkedList.head.value).be.equal(2);
            });
            it('Delete first node out of two - size should be 1', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.delete(singlyLinkedList.head);
                expect(singlyLinkedList.size).be.equal(1);
            });
            it('Delete second node - first node next should be null', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.insertAfter(2);
                singlyLinkedList.delete(singlyLinkedList.head.next);
                expect(singlyLinkedList.head.next).be.null;
            });
            it('Delete last node - first node next should be null', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.insertAfter(2);
                singlyLinkedList.delete(singlyLinkedList.tail);
                expect(singlyLinkedList.head.next).be.null;
            });
            it('Delete first node out of two from the node - size should be 1', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.head.delete();
                expect(singlyLinkedList.size).be.equal(1);
            });
            it('Delete first node out of two from the node - first node value should be 2', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.insert(2);
                singlyLinkedList.head.delete();
                expect(singlyLinkedList.head.value).be.equal(2);
            });
            it('Delete last node from the list - first node next should be null', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.insertAfter(2);
                singlyLinkedList.tail.delete();
                expect(singlyLinkedList.head.next).be.null;
            });
            it('Deleting only node should result in size 0', () => {
                singlyLinkedList.insert(1);
                singlyLinkedList.head.delete();
                expect(singlyLinkedList.size).to.equal(0);
            });
        });
    });
});
