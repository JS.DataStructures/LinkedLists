'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DoublyLinkedList = exports.SinglyLinkedList = undefined;

var _SinglyLinkedList = require('./SinglyLinkedList');

var _SinglyLinkedList2 = _interopRequireDefault(_SinglyLinkedList);

var _DoublyLinkedList = require('./DoublyLinkedList');

var _DoublyLinkedList2 = _interopRequireDefault(_DoublyLinkedList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.SinglyLinkedList = _SinglyLinkedList2.default;
exports.DoublyLinkedList = _DoublyLinkedList2.default;