import chai from 'chai';
import DoublyLinkedList from '../src/DoublyLinkedList';

const expect = chai.expect;

describe('LinkedLists', () => {
    describe('DoublyLinkedList', () => {
        let doublyLinkedList;
        beforeEach(() => {
            doublyLinkedList = new DoublyLinkedList();
        });
        describe('Insertion Tests', () => {
            it('should be able to construct', () => {
                expect(doublyLinkedList).be.not.null && expect(doublyLinkedList).be.not.undefined;
            });
            it('should construct with null first node', () => {
                expect(doublyLinkedList.head).be.null;
            });
            it('should construct with null last node', () => {
                expect(doublyLinkedList.tail).be.null;
            });
            it('should be able to insert an element as first node', () => {
                doublyLinkedList.insert(1);
                expect(doublyLinkedList.head.value).be.equal(1);
            });
            it('first node post constructor should be the last node', () => {
                doublyLinkedList.insert(1);
                expect(doublyLinkedList.head).be.equal(doublyLinkedList.tail);
            });
            it('first node post constructor should not have a next node', () => {
                doublyLinkedList.insert(1);
                expect(doublyLinkedList.tail.next).be.null;
            });
            it('first node post constructor should result in size 1', () => {
                doublyLinkedList.insert(1);
                expect(doublyLinkedList.size).be.equal(1);
            });
            it('second node should be sibling of first with second value', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.head.next.value).be.equal(2);
            });
            it('first node should be sibling of second with first value', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.tail.previous.value).be.equal(1);
            });
            it('second node should be last node with second value', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.tail.value).be.equal(2);
            });
            it('second node should result in size 2', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.size).be.equal(2);
            });
            it('second node inserted after first node should be sibling of first with second value', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.head.next.value).be.equal(2);
            });
            it('second node inserted after first node should be last node with second value', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                expect(doublyLinkedList.tail.value).be.equal(2);
            });
            it('second node inserted after using the first node should be a sibling of the first node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertAfter(2);
                expect(doublyLinkedList.head.next.value).be.equal(2);
            });
            it('second node inserted before using the first node should be a sibling of the first node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertBefore(2);
                expect(doublyLinkedList.tail.previous.value).be.equal(2);
            });
            it('second node inserted as head should result in head value being 2', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insertHead(2);
                expect(doublyLinkedList.head.value).be.equal(2);
            });
            it('second node inserted as head should result in head sibling value being 1', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insertHead(2);
                expect(doublyLinkedList.head.next.value).be.equal(1);
            });
            it('second node inserted as head should result in tail value being 1', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insertHead(2);
                expect(doublyLinkedList.tail.value).be.equal(1);
            });
        });

        describe('Find Tests', () => {
            it('Find an existing node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.insert(3);
                doublyLinkedList.insert(4);
                expect(doublyLinkedList.find(3)).be.not.null;
            });
            it('Find a non-existent node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.insert(3);
                doublyLinkedList.insert(4);
                expect(doublyLinkedList.find(5)).be.null;
            });
        });

        describe('Update Tests', () => {
            it('Update an existing node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.insert(3);
                doublyLinkedList.insert(4);
                const dataNode = doublyLinkedList.find(3);
                dataNode.value = 5;
                expect(doublyLinkedList.find(5)).be.not.null;
            });
        });

        describe('Delete Tests', () => {
            it('Delete first and only node - first node should be null', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.delete(doublyLinkedList.head);
                expect(doublyLinkedList.head).be.null;
            });
            it('Delete first and only node - size should be 0', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.delete(doublyLinkedList.head);
                expect(doublyLinkedList.size).be.equal(0);
            });
            it('Delete first node out of two - second node should become first node', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.delete(doublyLinkedList.head);
                expect(doublyLinkedList.head.value).be.equal(2);
            });
            it('Delete first node out of two - size should be 1', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.delete(doublyLinkedList.head);
                expect(doublyLinkedList.size).be.equal(1);
            });
            it('Delete second node - first node next should be null', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertAfter(2);
                doublyLinkedList.delete(doublyLinkedList.head.next);
                expect(doublyLinkedList.head.next).be.null;
            });
            it('Delete last node - first node next should be null', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertAfter(2);
                doublyLinkedList.delete(doublyLinkedList.tail);
                expect(doublyLinkedList.head.next).be.null;
            });
            it('Delete first node out of two from the node - size should be 1', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.head.delete();
                expect(doublyLinkedList.size).be.equal(1);
            });
            it('Delete first node out of two from the node - first node value should be 2', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.insert(2);
                doublyLinkedList.head.delete();
                expect(doublyLinkedList.head.value).be.equal(2);
            });
            it('Delete last node from the node - first node next should be null', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertAfter(2);
                doublyLinkedList.tail.delete();
                expect(doublyLinkedList.head.next).be.null;
            });
            it('Deleting all nodes should result in size 0', () => {
                doublyLinkedList.insert(1);
                doublyLinkedList.head.insertAfter(2);
                doublyLinkedList.head.delete();
                doublyLinkedList.tail.delete();
                expect(doublyLinkedList.size).to.equal(0);
            });
        });
    });
});
